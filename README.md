這是[中醫笈成](https://jicheng.tw/tcm/index.html)網站的原始檔，可透過[笈成站台產生器](https://gitlab.com/jicheng/jc.ssg)建立靜態網站。

## 進階設定
可更改 `config.yaml` 調整笈成站台產生器的設定。

自行架設的站台由於位在不同網域，需要更動一些外部資源引用設定，才能使相關功能正常運作：
* `theme_assets/default/hanzi_script_url`: 笈成缺字處理腳本，預設載入笈成檢字系統的腳本。一般不須更動也能正常運作，除非要改用自行架設的組字引擎；也可以刪除此值以停用缺字處理技術。
* `theme_assets/default/hanzi_fonts`: 網路字體列表，預設載入笈成檢字系統的網路字體。自行架設的站台會受同源政策限制而無法載入這些網路字體，可調整設定改用自架網站提供的網路字體，或刪除此值以停用網路字體支援。
* `theme_assets/default/vcs_url_prefix`: 版本庫連結位址，預設為笈成版本庫的對應頁面。可調整設定改為連往自行建立的版本庫，或刪除此值以停用版本庫連結。
* `theme_assets/default/google_analytics_id`: Google 網站流量分析 ID。自行架設站台時原數值無法作用，可調整設定改用自行申請的 ID，或刪除此值以停用網站流量分析功能。
* `theme_assets/default/google_cse_script_url`: Google 自訂搜尋引擎腳本，預設是檢索中醫笈成本站。可調整設定改用自行建立的自訂搜尋引擎腳本，或刪除此值以停用此功能。
* `theme_assets/default/google_site_search_root`: Google 網站搜尋根目錄，預設是檢索中醫笈成本站（停用自訂搜尋引擎時生效）。可調整設定改為自架網站，或刪除此值以停用此功能。
* `theme_assets/default/bing_site_search_root`: Bing 網站搜尋根目錄，預設是檢索中醫笈成本站（停用自訂搜尋引擎時生效）。可調整設定改為自架網站，或刪除此值以停用此功能。

## 自訂模版
可在 `themes/` 目錄下加入自製網站模版（格式可仿照預設模版 `themes/default` 撰寫），並修改 `config.yaml` 的 `theme` 及 `theme_assets/*/` 設定值以使用之。
